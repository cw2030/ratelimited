package main

import (
	"crypto/rand"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

func main() {
	http.Handle("/", wrappedHttp(index))
	http.Handle("/download", wrappedHttp(download))
	http.Handle("/favicon.ico", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

	}))

	if err := http.ListenAndServe(":8080", nil); err != nil {
		fmt.Printf("%s\n", err)
	}
}

/**
包装http处理逻辑部分，增加日志和未知异常处理
 */
func wrappedHttp(f func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				fmt.Printf("Request Error :%s, error:%v", r.RequestURI, err)
			}
		}()
		logger(f, w, r)
	}
}

/**
生成首页测试代码
 */
func index(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("<html><form action='/download'>请选择测试文件大小(0.1-10)：<input type='text' id='size' name= 'size'/>(M) <br> <input type='submit'/></form></html>"))
}

/**
文件下载处理入口
 */
func download(w http.ResponseWriter, r *http.Request) {
	var err error
	if err = r.ParseForm(); err != nil {
		w.Write([]byte(err.Error()))
		fmt.Printf("Request.ParseForm fail.")
		return
	}
	fileSize := r.Form.Get("size")
	fileSizeFloat, err := strconv.ParseFloat(fileSize, 64)
	if err != nil {
		fmt.Printf("fileSize:%s, error:%s", fileSize, err.Error())
		w.Write([]byte("Error fileszie, please reset."))
		return
	}
	if fileSizeFloat > 10 || fileSizeFloat < 0.1 {
		w.Write([]byte("测试文件的大小必须在0.1M到10M之间"))
		return
	}

	rateLimited(int32(fileSizeFloat * 1024 * 1024), w)
}

/**
限制下载指定大小的文件
 */
func rateLimited(fileSize int32, w http.ResponseWriter)  {
	var err error
	var total int32
	var b []byte

	rate := time.Second / 20
	ticker := time.NewTicker(rate)

	w.Header().Add("Content-Type", "application/octet-stream")
	w.Header().Add("Content-Disposition", "attachment;filename=test.txt")

	for _ = range ticker.C {
		remainBytes := fileSize - total
		switch {
		case remainBytes <= 0:
			goto END
		case remainBytes < 1024:
			if b, err = RandStr(remainBytes); err != nil {
				goto END
			} else {
				w.Write(b)
				total += remainBytes
			}
		case remainBytes >= 1024:
			if b, err := RandStr(1024); err != nil {
				goto END
			} else {
				w.Write(b)
				total += 1024
			}
		}
	}
END:
	if err != nil {
		fmt.Printf("download fail,error: %s\n", err)
	}
	ticker.Stop()
	ticker = nil
}

/**
记录业务访问日志
 */
func logger(f func(http.ResponseWriter, *http.Request), w http.ResponseWriter, r *http.Request) {
	startDt := time.Now()
	f(w, r)
	fmt.Printf("Request:%s, processTime:%.2fs\n", r.RequestURI, time.Now().Sub(startDt).Seconds())
}

/**
生成指定大小随机字节数
 */
func RandStr(size int32) ([]byte, error) {
	if size < 0 {
		return nil, errors.New("size must great than or equals to 0")
	}
	if size > 1024 {
		return nil, errors.New("size must less than or equals to 1024")
	}
	b := make([]byte, size)
	_, err := rand.Read(b)
	return b, err
}
